var map = L.map('map').setView([38.141037, -0.373535], 10);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
    maxZoom: 18,
    id: 'alexruba.64ce3164',
    accessToken: 'pk.eyJ1IjoiYWxleHJ1YmEiLCJhIjoiYTA4NGUzZmMwNWZiMDA0ZTAyYTA1NDM1ODRiYmRiMmYifQ.-WTLdlBcr3SLdCacaDc7Tg'
}).addTo(map);

var vesselIcon = L.icon({
    iconUrl: 'img/vessel.png',
});

// We add some markers
var shipsInfo;

$.ajax({
	type: "GET",
	url: "data/ships.js",
	async: false,
	dataType: "json",
	success: function(data){
		shipsInfo = data;
		checkShips();
	},
	error: function (request, status, error) {
        alert(request.responseText);
        alert(error);
    }
});

function checkShips() {
	var marker;

	for(var i = 0; i < shipsInfo.data.length; i++)
	{
		marker = L.marker([shipsInfo.data[i].Latitude, shipsInfo.data[i].Longitude], {icon: vesselIcon}).addTo(map);
		marker.bindPopup("<b>" + shipsInfo.data[i].ShipName + "</b><br>Destino: " + shipsInfo.data[i].Destination + "<br>" + shipsInfo.data[i].Latitude + " " + shipsInfo.data[i].Longitude);
	}
}

