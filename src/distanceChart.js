app.directive('distanceChart', function() {
  
  return {
    link: link,
    restrict: 'E',
    scope: {
      data: '=',
      product: '=',
      day: '='
    }
  }

  function link(scope, el, attrs) {

    // watch for data changes and re-render
    scope.$watch('product', function() {
      return scope.render(scope.data);
    }, true);

    // watch for data changes and re-render
    scope.$watch('day', function() {
      return scope.render(scope.data);
    }, true);

    var margin = {top: 20, right: 20, bottom: 30, left: 40},
          width = 960 - margin.left - margin.right,
          height = 500 - margin.top - margin.bottom;

    var x = d3.scale.linear()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var color = function(kg)
    {
      if(kg === "Red")
        return "#C0392B";
      else if(kg === "Yellow")
        return "#F1C40F";
      else
        return "#2ECC71";
    }

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    var svg = d3.select(el[0]).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");
    
    function mouseover(p) {
      svg.append("text")
          .attr("x", width - 300)
          .attr("y", 0)
          .style("text-align", "center")
          .style("fill", "black")
          .attr("class","lead")
          .text(p.STORE);

      var g = d3.select(this).node();
      d3.select(g).attr("r", 18);
    }

    function mouseout(p) {
      var g = d3.select(this).node().parentNode;
      d3.select(g).selectAll("text.lead").remove();

      var circle = d3.select(this).node();
      d3.select(circle).attr("r", 10);

    }

    scope.render = function(data) {

      svg.selectAll('*').remove();

      if (typeof data === 'undefined' || data === null || data.length === 0) return;
      var finalData = [];
      data.forEach(function(d, i) {
        if(d.PRODUCT === scope.product && d.KM !== 0 &&
          (d.DAY == scope.day || d.DAY == scope.day + 1 || d.DAY == scope.day + 2) && 
          d.EKG != 0.0)
          finalData.push(d);
      });

      if(finalData.length == 0)
      {
         svg.append("text")
          .attr("x", width/4.5)
          .attr("y", height/2)
          .style("text-align", "center")
          .style("fill", "black")
          .attr("class","lead")
          .text("No se encontraron establecimientos. Prueba con otro día");
      }
      else
      {
        var start_x = 0;
        var end_x = d3.max(finalData, function(d) { return d.KM; });
        var start_y = 0;
        var end_y = d3.max(finalData, function(d) { return d.EKG })
        x.domain([start_x, end_x]);
        y.domain([start_y, end_y]);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
          .append("text")
            .attr("class", "label")
            .attr("x", width)
            .attr("y", -6)
            .style("text-anchor", "end")
            .text("Kilometers");

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
          .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text("€/Kg")

          svg.selectAll(".dot")
            .data(finalData)
          .enter().append("circle")
            .attr("class", "dot")
            .attr("r", 10)
            .attr("cx", function(d) { return x(d.KM); })
            .attr("cy", function(d) { return y(d.EKG); })
            .style("fill", function(d) { return color(d.KG); })
            .attr("id", function(d){ return d.STORE;})
            .on("mouseover", mouseover)
            .on("mouseout", mouseout);

      }
     

      };

    };
});