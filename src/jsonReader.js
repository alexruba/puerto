d3.json("data/lonja-year.js", function(error, data) {
	if (error) return console.warn(error);

	var data = data.data;

	x.domain([start_year, end_year]);

	var xScale = d3.scale.linear()
		.domain([start_year, end_year])
		.range([0, width]);

	var rScale = d3.scale.linear()
		.domain([0, d3.max(data, function(d) { return d.KG; })])
		.range([0, 20]);
	
	svg.append("g")
		.attr("class", "x axis")
		.attr("transform", "translate(0," + 0 + ")")
		.call(xAxis);

	var indexAppearance = [];
	var lastJ = 0;
	var g;

	for (var j = 0; j < data.length; j++) 
	{
		// If it is a non-registered product, we add a row for it
		if(indexAppearance[indexAppearance.length - 1] != data[j].ID)
		{
			indexAppearance.push(data[j].ID);
			lastJ++;

			g = svg.append("g");

			g.append("text")
				.attr("y", lastJ*20+25)
				.attr("x",width+20)
				.attr("class","label")
				.text(truncate(data[j]['NAME'],30,"..."))
				.style("fill", function(d) { return c(lastJ); })
				.on("mouseover", mouseover)
				.on("mouseout", mouseout);
		}

		// On each elem we have, we add a circle and a text 
		var circles = g.append("circle");
		var text = g.append("text");

		circles
			.attr("cx", xScale(data[j].YEAR))
			.attr("cy", lastJ*20+20)
			.attr("r", rScale(data[j].KG))
			.style("fill", c(lastJ));

		text
			.attr("y", lastJ*20+25)
			.attr("x", xScale(data[j].YEAR))
			.attr("class","value")
			.text(data[j].KG)
			.style("fill", c(lastJ))
			.style("display","none");

		
	};
});