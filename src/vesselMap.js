app.directive('vesselMap', ['$compile', function($compile) {
	return {
		link: link,
		restrict: 'E',
		scope: {
			data: '=',
			icon: '@',
			id: '@',
			longitude: '@',
			latitude: '@',
			altitude: '@'
		}
	}

	function link(scope, el, attrs) {

		// watch for data changes and re-render
		scope.$watch('data', function() {
		  return scope.render(scope.data);
		}, true);

		var map = L.map(scope.id).setView([scope.latitude, scope.longitude], scope.altitude);

		L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token={accessToken}', {
		    attribution: 'Map data &copy; <a href="http://openstreetmap.org">OpenStreetMap</a> contributors, <a href="http://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="http://mapbox.com">Mapbox</a>',
		    maxZoom: 18,
		    id: 'alexruba.64ce3164',
		    accessToken: 'pk.eyJ1IjoiYWxleHJ1YmEiLCJhIjoiYTA4NGUzZmMwNWZiMDA0ZTAyYTA1NDM1ODRiYmRiMmYifQ.-WTLdlBcr3SLdCacaDc7Tg'
		}).addTo(map);

		var legend = L.control({position: 'bottomright'});
		legend.onAdd = function (map) {
		    var div = L.DomUtil.create('div', 'pieDiv');
		    return div;
		};
		legend.addTo(map);

		/*$('#map').on('click', '.trigger', function() {
		    var ship = $(this).attr('id');
  			$("#pie").attr('ship', ship);
		});*/

		scope.render = function(data) {
			var marker;
			var vesselIcon = L.icon({
			    iconUrl: scope.icon,
			});

			L.VirtualGrid = L.FeatureGroup.extend({
			  include: L.Mixin.Events,
			  value: 1,
			  options: {
			    cellSize: 128,
			    delayFactor: 1,
			    blueStyle: {
			      stroke: true,
			      color: '#fff',
			      dashArray: null,
			      lineCap: null,
			      lineJoin: null,
			      weight: 2,
			      opacity: 0.2,

			      fill: true,
			      fillColor: '#fff', //same as color by default
			      fillOpacity: 0.1,

			      clickable: true
			    },
			    yellowStyle: {
			      stroke: true,
			      color: '#fff',
			      dashArray: null,
			      lineCap: null,
			      lineJoin: null,
			      weight: 2,
			      opacity: 0.2,

			      fill: true,
			      fillColor: '#F1C40F', //same as color by default
			      fillOpacity: 0.5,

			      clickable: true
			    },
			    orangeStyle: {
			      stroke: true,
			      color: '#fff',
			      dashArray: null,
			      lineCap: null,
			      lineJoin: null,
			      weight: 2,
			      opacity: 0.2,

			      fill: true,
			      fillColor: '#FF9933', //same as color by default
			      fillOpacity: 0.5,

			      clickable: true
			    },
			    redStyle: {
			      stroke: true,
			      color: '#fff',
			      dashArray: null,
			      lineCap: null,
			      lineJoin: null,
			      weight: 2,
			      opacity: 0.2,

			      fill: true,
			      fillColor: '#C0392B', //same as color by default
			      fillOpacity: 0.5,

			      clickable: true
			    }

			  },
			  initialize: function(options){
			    L.Util.setOptions(this, options);
			    L.FeatureGroup.prototype.initialize.call(this, [], options);
			  },
			  onAdd: function(map){
			    L.FeatureGroup.prototype.onAdd.call(this, map);
			    this._map = map;
			    this._cells = [];
			    this._setupGrid(map.getBounds());

			    map.on("move", this._moveHandler, this);
			    map.on("zoomend", this._zoomHandler, this);
			    map.on("resize", this._resizeHandler, this);
			  },
			  onRemove: function(map){
			    L.FeatureGroup.prototype.onRemove.call(this, map);
			    map.off("move", this._moveHandler, this);
			    map.off("zoomend", this._zoomHandler, this);
			    map.off("resize", this._resizeHandler, this);
			  },
			  _clearLayer: function(e) {
			    this._cells = [];
			  },
			  _moveHandler: function(e){
			    this._renderCells(e.target.getBounds());
			  },
			  _zoomHandler: function(e){
			    this.clearLayers();
			    this._renderCells(e.target.getBounds());
			  },
			  _renderCells: function(bounds) {
			    var cells = this._cellsInBounds(bounds);
			    this.fire("newcells", cells);
			    for (var i = cells.length - 1; i >= 0; i--) {
			      var cell = cells[i];
			      if(this._loadedCells.indexOf(cell.id) === -1){
			        (function(cell, i){
				       	var cellRectangle = L.rectangle(cell.bounds, this.options.blueStyle);
				       	var ships = 0;

				       	if(scope.data.length != 0)
				       	{
				       		scope.data.forEach(function(d, i){
				       	
								var coords = L.latLng(d.Latitude, d.Longitude),
							    bounds = L.latLngBounds(coords, coords);

					       		if(cellRectangle.getBounds().contains(bounds))
					       		{
					       			ships++;
					       		}
				       		});
				       	}
				        if(ships == 0)
				          setTimeout(this.addLayer.bind(this, L.rectangle(cell.bounds, this.options.blueStyle)), this.options.delayFactor*i);
				        else if(ships == 1)
				          setTimeout(this.addLayer.bind(this, L.rectangle(cell.bounds, this.options.yellowStyle)), this.options.delayFactor*i);
				        else if(ships == 2)
				          setTimeout(this.addLayer.bind(this, L.rectangle(cell.bounds, this.options.orangeStyle)), this.options.delayFactor*i);
				        else
				          setTimeout(this.addLayer.bind(this, L.rectangle(cell.bounds, this.options.redStyle)), this.options.delayFactor*i);
			        }.bind(this))(cell, i);
			        this._loadedCells.push(cell.id);
			      }
			    }
			  },
			  _resizeHandler: function(e) {
			    this._setupSize();
			  },
			  _setupSize: function(){
			    this._rows = Math.ceil(this._map.getSize().x / this._cellSize);
			    this._cols = Math.ceil(this._map.getSize().y / this._cellSize);
			  },
			  _setupGrid: function(bounds){
			    this._origin = this._map.project(bounds.getNorthWest());
			    this._cellSize = this.options.cellSize;
			    this._setupSize();
			    this._loadedCells = [];
			    this.clearLayers();
			    this._renderCells(bounds);
			  },
			  _cellPoint:function(row, col){
			    var x = this._origin.x + (row*this._cellSize);
			    var y = this._origin.y + (col*this._cellSize);
			    return new L.Point(x, y);
			  },
			  _cellExtent: function(row, col){
			    var swPoint = this._cellPoint(row, col);
			    var nePoint = this._cellPoint(row-1, col-1);
			    var sw = this._map.unproject(swPoint);
			    var ne = this._map.unproject(nePoint);
			    return new L.LatLngBounds(ne, sw);
			  },
			  _cellsInBounds: function(bounds){
			    var offset = this._map.project(bounds.getNorthWest());
			    var center = bounds.getCenter();
			    var offsetX = this._origin.x - offset.x;
			    var offsetY = this._origin.y - offset.y;
			    var offsetRows = Math.round(offsetX / this._cellSize);
			    var offsetCols = Math.round(offsetY / this._cellSize);
			    var cells = [];

			    for (var i = 0; i <= this._rows; i++) {
			      for (var j = 0; j <= this._cols; j++) {
			        var row = i-offsetRows;
			        var col = j-offsetCols;
			        var cellBounds = this._cellExtent(row, col);
			        var cellId = row+":"+col;
			        cells.push({
			          id: cellId,
			          bounds: cellBounds,
			          distance:cellBounds.getCenter().distanceTo(center)
			        });
			      }
			    }
			    cells.sort(function (a, b) {
			      return a.distance - b.distance;
			    });
			    return cells;
			  }
			});

			L.virtualGrid = function(url, options){
			  return new L.VirtualGrid(options);
			};

			L.virtualGrid({
			  cellSize: 128
			}).addTo(map);

			for(var i = 0; i < data.length; i++)
			{
				marker = L.marker([data[i].Latitude, data[i].Longitude], {icon: vesselIcon}).addTo(map); 
				var html = "<b>" + data[i].ShipName + "</b><br>Destino: " +
								 data[i].Destination + "<br>" + data[i].Latitude + " " + data[i].Longitude + "<br>";
				var htmlMod = $compile("<button id='" + data[i].ShipName + "' href='#' class='toggle btn btn-primary' ng-click=\"ship='" +
								data[i].ShipName + "';\">" + html + "...</button>")(scope.$parent);
				marker.bindPopup(htmlMod[0]);
			}
			
		}
	}
}]);