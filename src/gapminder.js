app.directive('gapminder', function() {
  
  return {
    link: link,
    restrict: 'E',
    scope: {
      data: '=',
      xmeasure: '=',
      ymeasure: '=',
      vmeasure: '=',
      year: '=',
      playing: '='
    }
  }

  function link(scope, el, attrs) {

    // watch for data changes and re-render
    scope.$watch('xmeasure', function() {
      return scope.render(scope.data);
    }, true);

    // watch for data changes and re-render
    scope.$watch('ymeasure', function() {
      return scope.render(scope.data);
    }, true);

    // watch for data changes and re-render
    scope.$watch('vmeasure', function() {
      return scope.render(scope.data);
    }, true);

    // watch for data changes and re-render
    scope.$watch('year', function() {
      return scope.render(scope.data);
    }, true);

    // watch for data changes and re-render
    scope.$watch('data', function() {
      return scope.render(scope.data);
    }, true);

    var margin = {top: 20, right: 20, bottom: 30, left: 40},
          width = 960 - margin.left - margin.right,
          height = 500 - margin.top - margin.bottom;

    var x = d3.scale.linear()
        .range([0, width]);

    var y = d3.scale.linear()
        .range([height, 0]);

    var color = d3.scale.category20c();

    var xAxis = d3.svg.axis()
        .scale(x)
        .orient("bottom");

    var yAxis = d3.svg.axis()
        .scale(y)
        .orient("left");

    var svg = d3.select(el[0]).append("svg")
        .attr("width", width + margin.left + margin.right)
        .attr("height", height + margin.top + margin.bottom)
      .append("g")
        .attr("transform", "translate(" + margin.left + "," + margin.top + ")");

    function mouseover(p) {

      var textValue = "";

      if(typeof p.SHIP === 'undefined')
        textValue = p.NAME;
      else
        textValue = p.SHIP;

      svg.append("text")
          .attr("x", width - 300)
          .attr("y", 0)
          .style("text-align", "center")
          .style("fill", "black")
          .attr("class","lead")
          .text(textValue);

      var g = d3.select(this).node();
      d3.select(g).attr("r", d3.select(g).attr("r") * 2);
    }

    function mouseout(p) {
      var g = d3.select(this).node().parentNode;
      d3.select(g).selectAll("text.lead").remove();

      var circle = d3.select(this).node();
      d3.select(circle).attr("r", d3.select(circle).attr("r") / 2);

    }

    scope.render = function(data) {

      svg.selectAll('*').remove();
      
      if (typeof data === 'undefined' || data === null || data.length === 0 ||
          typeof scope.xmeasure === 'undefined' || scope.xmeasure === null || scope.xmeasure === "" ||
          typeof scope.ymeasure === 'undefined' || scope.ymeasure === null || scope.ymeasure === "" ||
          typeof scope.vmeasure === 'undefined' || scope.vmeasure === null || scope.vmeasure === "" ||
          typeof scope.year === 'undefined' || scope.year === null || scope.year === "") return;

      var finalData = [];
      data.forEach(function(d, i) {
        if(d.YEAR == scope.year)
          finalData.push(d);
      });

      if(finalData.length == 0)
      {
         svg.append("text")
          .attr("x", width/4.5)
          .attr("y", height/2)
          .style("text-align", "center")
          .style("fill", "black")
          .attr("class","lead")
          .text("No se encontraron datos. Prueba con otros parámetros.");
      }
      else
      {
        var start_x = 0;
        var end_x = d3.max(finalData, function(d) { return d[scope.xmeasure]; });
        var start_y = 0;
        var end_y = d3.max(finalData, function(d) { return d[scope.ymeasure] })
        x.domain([start_x, end_x]);
        y.domain([start_y, end_y]);

        var rScale =  d3.scale.linear()
                        .domain([0, d3.max(finalData, function(d) {if(scope.vmeasure === 'Benefits')
                                                                     return d.KG * d.EKG;
                                                                    else 
                                                                     return d[scope.vmeasure]; })])
                        .range([0, 20]);

        svg.append("g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + height + ")")
            .call(xAxis)
          .append("text")
            .attr("class", "label")
            .attr("x", width)
            .attr("y", -6)
            .style("text-anchor", "end")
            .text(scope.xmeasure);

        svg.append("g")
            .attr("class", "y axis")
            .call(yAxis)
          .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text(scope.ymeasure)

          svg.selectAll(".dot")
            .data(finalData)
          .enter().append("circle")
            .attr("class", "dot")
            .transition()
            .attr("r", function(d){ if(scope.vmeasure !== 'Benefits')
                                      return rScale(d[scope.vmeasure]);
                                    else
                                      return rScale(d.KG * d.EKG);})
            .duration(1000)
            .attr("cx", function(d) { return x(d[scope.xmeasure]); })
            .attr("cy", function(d) { return y(d[scope.ymeasure]); })
            .style("fill", function(d) { return color(d[scope.ymeasure]); })
            .on("mouseover", mouseover)
            .on("mouseout", mouseout);
      }

      };

    };
});