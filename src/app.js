// create module for custom directives
var app = angular.module('app', [])
	app.controller('MainController', ['$scope', '$http', function($scope, $http){
		$scope.site = 1;
	}]);
	app.controller('LonjaChartController', ['$scope', '$http', function($scope, $http){
		$scope.measure = "EKG";
		$scope.yearData = [];
		$scope.monthData = [];
		$scope.dataTime = 'y';
		$scope.yearToShow = -1;
		$scope.years = ['2005', '2006', '2007', '2008', '2009', '2010',
						'2011', '2012', '2013', '2014', '2015'];

		$scope.selectedIndex = -1; // Whatever the default selected index is, use -1 for no selection

		$scope.itemClicked = function ($index) {
		    $scope.selectedIndex = $index;
		    $scope.yearToShow = $scope.years[$index];
		    $scope.dataTime = 'm';
		};

		$scope.changeTime = function(time) {
			$scope.dataTime = time;
			$scope.selectedIndex = -1;
		}

		$http.get('data/lonja-year.js').success(function(data) { 
			$scope.yearData = data.data;
		});
		$http.get('data/lonja-month.js').success(function(data) { 
			$scope.monthData = data.data;
		});
	}]);

	app.controller('VesselMapController', ['$scope', '$http', function($scope, $http) {
		$scope.shipsData = [];
		$scope.vesselProductData = [];
		$scope.ship = "";

		$http.get('data/ships.js').success(function(data) { 
			$scope.shipsData = data.data;
		});
		
		$http.get('data/lonja-ship.js').success(function(data) { 
			$scope.vesselProductData = data.data;
		});
	}]);

	app.controller('ProductsDataController', ['$scope', '$http', function($scope, $http) {
		$scope.productsYear = [];
		$scope.yearToShow = -1;
		$scope.dataTime = 'y';
		$scope.productsMonth = [];
		$scope.years = ['2005', '2006', '2007', '2008', '2009', '2010',
						'2011', '2012', '2013', '2014', '2015'];

		$scope.itemClicked = function ($index) {
		    $scope.selectedIndex = $index;
		    $scope.yearToShow = $scope.years[$index];
		    $scope.dataTime = 'm';
		};

		$scope.changeTime = function(time) {
			$scope.dataTime = time;
			$scope.selectedIndex = -1;
		}

		$http.get('data/lonja-year.js').success(function(data) { 
			$scope.productsYear = data.data;
		});
		$http.get('data/lonja-month.js').success(function(data) { 
			$scope.productsMonth = data.data;
		});
	}]);

	app.controller('DistanceChartController', ['$scope', '$http', function($scope, $http) {
		$scope.distanceData = [];
		$scope.products = [];
		$scope.days = [1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31];
		$scope.selectedProduct = "BESUGO";
		$scope.selectedProductIndex = -1;
		$scope.selectedDay = 1;
		$scope.selectedDayIndex = -1;

		$scope.productClicked = function ($index) {
		    $scope.selectedProductIndex = $index;
		    $scope.selectedProduct = $scope.products[$index];
		};

		$scope.dayClicked = function ($index) {
		    $scope.selectedDayIndex = $index;
		    $scope.selectedDay = $scope.days[$index];
		};

		$http.get('data/distance.js').success(function(data) { 
			$scope.distanceData = data.data;
			$scope.distanceData.forEach(function(d,i) {
			if($.inArray(d.PRODUCT, $scope.products) == -1)
				$scope.products.push(d.PRODUCT);
			});
		});	
	}]);

	app.controller('GapminderController', ['$scope', '$http', function($scope, $http) {
		$scope.lonjaData = [];
		$scope.shipsData = [];
		$scope.data = [];
		$scope.xMeasure = ['KG', 'EKG'];
		$scope.yMeasure = ['KG', 'EKG'];
		$scope.vMeasure = ['KG', 'EKG', 'Benefits'];
		$scope.years = ['2005', '2006', '2007', '2008', '2009', '2010',
						'2011', '2012', '2013', '2014', '2015'];
		$scope.selectedXIndex = -1;
		$scope.selectedYIndex = -1;
		$scope.selectedVIndex = -1;
		$scope.selectedYearIndex = -1;
		$scope.selectedX = "";
		$scope.selectedY = "";
		$scope.selectedV = "";
		$scope.selectedYear = "";
		$scope.playing = false;
		$scope.refreshIntervalId = "";
		$scope.chartType = "P";

		$scope.xMeasureClicked = function ($index) {
		    $scope.selectedXIndex = $index;
		    $scope.selectedX = $scope.xMeasure[$index];
		};

		$scope.yMeasureClicked = function ($index) {
		    $scope.selectedYIndex = $index;
		    $scope.selectedY = $scope.yMeasure[$index];
		};

		$scope.vMeasureClicked = function ($index) {
		    $scope.selectedVIndex = $index;
		    $scope.selectedV = $scope.vMeasure[$index];
		};

		$scope.play = function(){
			$scope.playing = true;
	      	$scope.refreshIntervalId = 
	        setInterval(function() {
	    	  	if($scope.selectedYearIndex < $scope.years.length - 1)
	            {
	              	$scope.selectedYear ++;
   	            	$scope.selectedYearIndex ++;
	          	}
	          	else
	          	{
	          		$scope.selectedYear = $scope.years[0];
	          		$scope.selectedYearIndex = 0;
	          	}       
	          	$scope.$apply()
	          }, 3000);
			
		}

		$scope.pause = function(){
			$scope.playing = false;
			clearInterval($scope.refreshIntervalId);
		}

		$scope.yearClicked = function ($index) {
		    $scope.selectedYearIndex = $index;
		    $scope.selectedYear = $scope.years[$index];
		};

		$http.get('data/lonja-year.js').success(function(data) {
			$scope.lonjaData = data.data;
			$scope.data = data.data;
		});	

		$http.get('data/lonja-ship-globalinfo.js').success(function(data) { 
			$scope.shipsData = data.data;
		});	
	}]);