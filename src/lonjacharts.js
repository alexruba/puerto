app.directive('lonjaChart', function() {

	return {
		link: link,
		restrict: 'E',
		scope: {
			data: '='
		}
	}

	function link(scope, el, attrs) {
		// watch for data changes and re-render
		scope.$watch('data', function() {
		  return scope.render(scope.data);
		}, true);

		var margin = {top: 20, right: 200, bottom: 0, left: 20},
		width = 420,
		height = 2050;

		var end_year = 2015;
		var start_year = 2005;

		var c = d3.scale.category20c();

		var x = d3.scale.linear()
		.range([0, width]);

		var xAxis = d3.svg.axis()
		.scale(x)
		.orient("top");

		var formatYears = d3.format("0000");
		xAxis.tickFormat(formatYears);

		var svg = d3.select(el[0]).append("svg")
					.attr("width", width + margin.left + margin.right)
					.attr("height", height + margin.top + margin.bottom)
					.style("margin-left", margin.left + "px")
					.append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		function mouseover(p) {
			var g = d3.select(this).node().parentNode;
			d3.select(g).selectAll("circle").style("display","none");
			d3.select(g).selectAll("text.value").style("display","block");
		}

		function mouseout(p) {
			var g = d3.select(this).node().parentNode;
			d3.select(g).selectAll("circle").style("display","block");
			d3.select(g).selectAll("text.value").style("display","none");
		}

		function truncate(str, maxLength, suffix) {
			if(str.length > maxLength) {
				str = str.substring(0, maxLength + 1); 
				str = str.substring(0, Math.min(str.length, str.lastIndexOf(" ")));
				str = str + suffix;
			}
			return str;
		}

		scope.render = function(data) {
			svg.selectAll('*').remove();
			if(!data) return;

			var renderMeasure = attrs.measure;
			var timeData = attrs.datatime;

			var xScale;
			x.domain([start_year, end_year]);
			xScale = d3.scale.linear()
						.domain([start_year, end_year])
						.range([0, width]);
				
			var rScale = d3.scale.linear()
				.domain([0, d3.max(data, function(d) { return d[renderMeasure]; })])
				.range([0, 15]);
			
			svg.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + 0 + ")")
				.call(xAxis);

			var indexAppearance = [];
			var lastJ = 0;
			var g;

			for (var j = 0; j < data.length; j++) 
			{
				// If it is a non-registered product, we add a row for it
				if(indexAppearance[indexAppearance.length - 1] != data[j].NAME)
				{
					indexAppearance.push(data[j].NAME);
					lastJ++;

					g = svg.append("g");

					g.append("text")
						.attr("y", lastJ*20+25)
						.attr("x",width+20)
						.attr("class","label")
						.text(truncate(data[j]['NAME'],30,"..."))
						.style("fill", function(d) { return c(lastJ); })
						.on("mouseover", mouseover)
						.on("mouseout", mouseout);
				}

				// On each elem we have, we add a circle and a text 
				var circles = g.append("circle");
				var text = g.append("text");

				circles
					.attr("cx", xScale(data[j].YEAR))
					.attr("cy", lastJ*20+20)
					.attr("r", rScale(data[j][renderMeasure]))
					.style("fill", c(lastJ));

				text
					.attr("y", lastJ*20+25)
					.attr("x", xScale(data[j].YEAR))
					.attr("class","value")
					.text(data[j][renderMeasure].toFixed(2))
					.style("fill", c(lastJ))
					.style("display","none");
			};
		}
		
	}
})

app.directive('lonjaChartMonth', function() {

	return {
		link: link,
		restrict: 'E',
		scope: {
			data: '=',
			year: '='
		}
	}

	function link(scope, el, attrs) {

		// watch for data changes and re-render
		scope.$watch('data', function(newValue, oldValue) {
			scope.data = newValue;
		}, true);
		// watch for data changes and re-render
		scope.$watch('year', function(newValue, oldValue) {
			return scope.render(scope.data);
		}, true);

		var margin = {top: 20, right: 200, bottom: 0, left: 20},
		width = 420,
		height = 2050;

		var end_month = 12;
		var start_month = 1;

		var c = d3.scale.category20c();

		var x = d3.scale.linear()
		.range([0, width]);

		var xAxis = d3.svg.axis()
		.scale(x)
		.orient("top");

		var formatYears = d3.format("0000");
		xAxis.tickFormat(formatYears);

		var svg = d3.select(el[0]).append("svg")
					.attr("width", width + margin.left + margin.right)
					.attr("height", height + margin.top + margin.bottom)
					.style("margin-left", margin.left + "px")
					.append("g")
					.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

		function mouseover(p) {
			var g = d3.select(this).node().parentNode;
			d3.select(g).selectAll("circle").style("display","none");
			d3.select(g).selectAll("text.value").style("display","block");
		}

		function mouseout(p) {
			var g = d3.select(this).node().parentNode;
			d3.select(g).selectAll("circle").style("display","block");
			d3.select(g).selectAll("text.value").style("display","none");
		}

		function truncate(str, maxLength, suffix) {
			if(str.length > maxLength) {
				str = str.substring(0, maxLength + 1); 
				str = str.substring(0, Math.min(str.length, str.lastIndexOf(" ")));
				str = str + suffix;
			}
			return str;
		}

		scope.render = function(data) {
			svg.selectAll('*').remove();
			if(!data) return;

			var renderMeasure = attrs.measure;

			var xScale;
			x.domain([start_month, end_month]);
			xScale = d3.scale.linear()
						.domain([start_month, end_month])
						.range([0, width]);
				
			var rScale = d3.scale.linear()
				.domain([0, d3.max(data, function(d) { if(d.YEAR == scope.year) return d[renderMeasure]; })])
				.range([0, 20]);
			
			svg.append("g")
				.attr("class", "x axis")
				.attr("transform", "translate(0," + 0 + ")")
				.call(xAxis);

			var indexAppearance = [];
			var lastJ = 0;
			var g;

			for (var j = 0; j < data.length; j++) 
			{
				if(data[j].YEAR != scope.year) continue;
				// If it is a non-registered product, we add a row for it
				if(indexAppearance[indexAppearance.length - 1] != data[j].NAME)
				{
					indexAppearance.push(data[j].NAME);
					lastJ++;

					g = svg.append("g");

					g.append("text")
						.attr("y", lastJ*20+25)
						.attr("x",width+20)
						.attr("class","label")
						.text(truncate(data[j]['NAME'],30,"..."))
						.style("fill", function(d) { return c(lastJ); })
						.on("mouseover", mouseover)
						.on("mouseout", mouseout);
				}

				// On each elem we have, we add a circle and a text 
				var circles = g.append("circle");
				var text = g.append("text");

				circles
					.attr("cx", xScale(data[j].MONTH))
					.attr("cy", lastJ*20+20)
					.attr("r", rScale(data[j][renderMeasure]))
					.style("fill", c(lastJ));

				text
					.attr("y", lastJ*20+25)
					.attr("x", xScale(data[j].MONTH))
					.attr("class","value")
					.text(data[j][renderMeasure])
					.style("fill", c(lastJ))
					.style("display","none");
				
				
			};
		}
		
	}
})
;