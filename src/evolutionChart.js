app.directive('evolutionChartYear', function() {
  
  return {
    link: link,
    restrict: 'E',
    scope: {
      data: '='
    }
  }

  function link(scope, el, attrs) {

    // watch for data changes and re-render
    scope.$watch('data', function() {
      return scope.render(scope.data);
    }, true);

    scope.render = function(data) 
    {
      if (typeof data === 'undefined' || data === null || data.length === 0) return;

      var color = d3.scale.category10();
      var
        WIDTH = 1000,
        HEIGHT = 500,
        MARGINS = {
            top: 50,
            right: 20,
            bottom: 50,
            left: 25
        },
        lSpace = WIDTH/500;
        xScale = d3.scale.linear().range([MARGINS.left, WIDTH - MARGINS.right]).domain([d3.min(scope.data, function(d) {
            return d.YEAR;
        }), d3.max(scope.data, function(d) {
            return d.YEAR;
        })]),
        yScale = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([d3.min(scope.data, function(d) {
            return d.EKG;
        }), d3.max(scope.data, function(d) {
            return d.EKG;
        })]),
        xAxis = d3.svg.axis()
                  .scale(xScale)
                  .tickFormat(d3.format('0f')),

        yAxis = d3.svg.axis()
                  .scale(yScale)
                  .orient("left");

        var vis = d3.select(el[0]).append("svg")
          .attr("width", WIDTH + MARGINS.left + MARGINS.right)
          .attr("height", HEIGHT + MARGINS.top + MARGINS.bottom)
          .style("margin-left", MARGINS.left + "px");

        vis.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + (HEIGHT - MARGINS.bottom) + ")")
            .call(xAxis);

        vis.append("svg:g")
            .attr("class", "y axis")
            .attr("transform", "translate(" + (MARGINS.left) + ",0)")
            .call(yAxis)
            .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("x", -50)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text("€/Kg")
            
        var lineGen = d3.svg.line()
                        .x(function(d) {
                            return xScale(d.YEAR);
                        })
                        .y(function(d) {
                            return yScale(d.EKG);
                        })
                        .interpolate("basis");

        var dataGroup = d3.nest()
                          .key(function(d) {return d.NAME;})
                          .entries(data);

        dataGroup.forEach(function(d,i) {

            vis.append('svg:path')
                .attr('d', lineGen(d.values))
                .attr('stroke', function(d,j) { 
                    return "hsl(" + 8 * 25 + ",10%,70%)";
                })
                .attr('stroke-width', 2)
                .attr('id', 'line_'+d.key)
                .attr('fill', 'none')
                .on('mouseover',function(){
                  d3.select(this).attr("stroke", "hsl(" + Math.random() * 360 + ",100%,20%)");
                  d3.selectAll("text.lead").text(d.key);
                })
                .on('mouseout', function(){
                  d3.select(this).attr("stroke", "hsl(" + 8 * 25 + ",10%,70%)");
                });

        });

        var priceEvolutionData = [10.2, 9.3, 10.7, 11.3, 10, 11.1, 10, 10, 10, 10.7, 9.8];
        var yearEvolutionData = [2005, 2006, 2007, 2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015];
        var arr = [];
          var len = priceEvolutionData.length;
          for (var i = 0; i < len; i++) {
              arr.push({
                  EKG: priceEvolutionData[i],
                  YEAR: yearEvolutionData[i]
              });
          }
        var evolutionData = {
          key : "EVOLUTION",
          values : arr
        };

        vis.append('svg:path')
                .attr('d', lineGen(evolutionData.values))
                .attr('stroke', function(d,j) { 
                    return "hsl(" + 14 * 25 + ",100%,35%)";
                })
                .attr('stroke-width', 4)
                .attr('id', 'line_' + evolutionData.key)
                .attr('fill', 'none')
                .on('mouseover',function(){
                  d3.selectAll("text.lead").text(evolutionData.key);
                })
                .on('mouseout', function(){
                  d3.select(this).attr("stroke", "hsl(" + 14 * 25 + ",100%,35%)");
                });

        vis.append("text")
          .attr("x", (lSpace/2))
          .attr("y", HEIGHT)
          .style("fill", "black")
          .attr("class","lead")
          .text("Selecciona un producto");
    }
  }
});

app.directive('evolutionChartMonth', function() {
  
  return {
    link: link,
    restrict: 'E',
    scope: {
      data: '=',
      year: '='
    }
  }

  function link(scope, el, attrs) {

    // watch for data changes and re-render
    scope.$watch('data', function(newValue, oldValue) {
      scope.data = newValue;
    }, true);

    // watch for data changes and re-render
    scope.$watch('year', function(newValue, oldValue) {
      return scope.render(scope.data);
    }, true);

    scope.render = function(data) 
    {
      if (typeof data === 'undefined' || data === null || data.length === 0) return;
      
      var finalData = [];
      data.forEach(function(d, i) {
        if(d.YEAR == scope.year)
          finalData.push(d);
      });

      if (typeof finalData === 'undefined' || finalData === null || finalData.length === 0) return;

      
      var color = d3.scale.category10();
      var
        WIDTH = 1000,
        HEIGHT = 500,
        MARGINS = {
            top: 50,
            right: 20,
            bottom: 50,
            left: 25
        },
        lSpace = WIDTH/500;
        xScale = d3.scale.linear().range([MARGINS.left, WIDTH - MARGINS.right]).domain([d3.min(finalData, function(d) {
            return d.MONTH;
        }), d3.max(finalData, function(d) {
            return d.MONTH;
        })]),
        yScale = d3.scale.linear().range([HEIGHT - MARGINS.top, MARGINS.bottom]).domain([d3.min(finalData, function(d) {
            return d.EKG;
        }), d3.max(finalData, function(d) {
            return d.EKG;
        })]),
        xAxis = d3.svg.axis()
                  .scale(xScale)
                  .tickFormat(d3.format('0f')),

        yAxis = d3.svg.axis()
                  .scale(yScale)
                  .orient("left");

        if($("#monthChart").length !== 0)
          d3.select(el[0]).selectAll("*").remove();
       
        var vis = d3.select(el[0]).append("svg")
                    .attr("id", "monthChart")
                    .attr("width", WIDTH + MARGINS.left + MARGINS.right)
                    .attr("height", HEIGHT + MARGINS.top + MARGINS.bottom)
                    .style("margin-left", MARGINS.left + "px");

        vis.append("svg:g")
            .attr("class", "x axis")
            .attr("transform", "translate(0," + (HEIGHT - MARGINS.bottom) + ")")
            .call(xAxis);

        vis.append("svg:g")
            .attr("class", "y axis")
            .attr("transform", "translate(" + (MARGINS.left) + ",0)")
            .call(yAxis)
            .append("text")
            .attr("class", "label")
            .attr("transform", "rotate(-90)")
            .attr("y", 6)
            .attr("x", -50)
            .attr("dy", ".71em")
            .style("text-anchor", "end")
            .text("€/Kg")
    
        var lineGen = d3.svg.line()
                        .x(function(d) {
                          return xScale(d.MONTH);
                        })
                        .y(function(d) {
                            return yScale(d.EKG);
                        })
                        .interpolate("basis");

        var dataGroup = d3.nest()
                          .key(function(d) {return d.NAME;})
                          .entries(finalData);

        dataGroup.forEach(function(d,i) {
            vis.append('svg:path')
              .attr('d', lineGen(d.values))
              .attr('stroke', function(d,j) { 
                  return "hsl(" + 8 * 25 + ",10%,70%)";
              })
              .attr('stroke-width', 2)
              .attr('id', 'line_'+d.key)
              .attr('fill', 'none')
              .on('mouseover',function(){
                d3.select(this).attr("stroke", "hsl(" + Math.random() * 360 + ",100%,20%)");
                d3.selectAll("text.lead").text(d.key);
              })
              .on('mouseout', function(){
                d3.select(this).attr("stroke", "hsl(" + 8 * 25 + ",10%,70%)");
              });
        });

        var priceEvolutionData = [10.2, 9.3, 10.7, 11.3, 10, 11.1, 10, 10, 10, 10.7, 9.8, 1.0];
        var monthEvolutionData = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12];
        var arr = [];
          var len = priceEvolutionData.length;
          for (var i = 0; i < len; i++) {
              arr.push({
                  EKG: priceEvolutionData[i],
                  MONTH: monthEvolutionData[i]
              });
          }
        var evolutionData = {
          key : "EVOLUTION",
          values : arr
        };

        vis.append('svg:path')
                .attr('d', lineGen(evolutionData.values))
                .attr('stroke', function(d,j) { 
                    return "hsl(" + 14 * 25 + ",100%,35%)";
                })
                .attr('stroke-width', 4)
                .attr('id', 'line_' + evolutionData.key)
                .attr('fill', 'none')
                .on('mouseover',function(){
                  d3.selectAll("text.lead").text(evolutionData.key);
                })
                .on('mouseout', function(){
                  d3.select(this).attr("stroke", "hsl(" + 14 * 25 + ",100%,35%)");
                });

        vis.append("text")
          .attr("x", (lSpace/2))
          .attr("y", HEIGHT)
          .style("fill", "black")
          .attr("class","lead")
          .text("Selecciona un producto");
    }
  }
});