app.directive('pieChart', function() {
  
  return {
    link: link,
    restrict: 'E',
    scope: {
      data: '=',
      ship: '='
    }
  }

  function link(scope, el, attrs) {

    scope.$watch('ship', function() {
      return scope.render(scope.data);
    }, true);

    // watch for data changes and re-render
    scope.$watch('data', function() {
      return scope.render(scope.data);
    }, true);

    var width = 480,
    height = 250,
    radius = Math.min(width, height) / 2;

    var color = d3.scale.category10();

    var arc = d3.svg.arc()
        .outerRadius(radius - 10)
        .innerRadius(radius - 70);

    var svg = d3.select(el[0]).append("svg")
        .attr("width", width)
        .attr("height", height)
        .append("g")
        .attr("transform", "translate(" + width / 2 + "," + height / 2 + ")");

    function mouseover(p) {
      svg.append("text")
          .attr("x", width - 200)
          .attr("y", 0)
          .style("text-align", "center")
          .style("fill", "black")
          .attr("class","lead")
          .text(p.STORE);

      var g = d3.select(this).node();
      d3.select(g).attr("r", 18);
    }

    function mouseout(p) {
      var g = d3.select(this).node().parentNode;
      d3.select(g).selectAll("text.lead").remove();

      var circle = d3.select(this).node();
      d3.select(circle).attr("r", 10);

    }

    scope.render = function(data) {
      svg.selectAll('*').remove();

      if (typeof data === 'undefined' || typeof scope.ship === 'undefined' ||
          scope.ship === "" || data === null || data.length === 0) 
            return;

      var shipProducts = new Array();
      var totalKg = 0;

      data.forEach(function(d) {
        if(d.SHIP === scope.ship)
        {
          shipProducts.push(d);
          totalKg += d.KG;
        }
      });

      shipProducts.sort(function(a, b){ return b.KG - a.KG;})
      shipProducts = shipProducts.slice(0, 3);
      shipProducts.forEach(function(d) {
        totalKg -= d.KG;
      });

      var restOfProducts = {
        KG: totalKg,
        EKG: 0.0,
        NAME: "REST",
        SHIP: scope.ship
      };

      shipProducts.push(restOfProducts);
      shipProducts.sort(function(a, b){ return b.KG - a.KG;})

      var pie = d3.layout.pie()
        .sort(null)
        .value(function(d) { return d.KG; });

      var g = svg.selectAll(".arc")
          .data(pie(shipProducts))
          .enter().append("g")
          .attr("class", "arc");

      g.append("path")
          .attr("d", arc)
          .style("fill", function(d, i) { return color(i); })
          .on("mouseenter", function(d) {
            d3.select(this)
               .attr("stroke","white")
               .transition()
               .duration(1000)
               .attr("d", arc)             
               .attr("stroke-width",3);
            })
          .on("mouseleave", function(d) {
              d3.select(this).transition()            
                 .attr("d", arc)
                 .attr("stroke","none");
          });

      g.append("text")
          .attr("transform", function(d) { return "translate(" + arc.centroid(d) + ")"; })
          .style("text-anchor", "middle")
          .attr("font-size", "8px")
          .text(function(d) { return d.data.NAME; });
      
      $(".pieDiv").append($("#pie")); 
    };
  }
});