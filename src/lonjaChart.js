function truncate(str, maxLength, suffix) {
	if(str.length > maxLength) {
		str = str.substring(0, maxLength + 1); 
		str = str.substring(0, Math.min(str.length, str.lastIndexOf(" ")));
		str = str + suffix;
	}
	return str;
}

var margin = {top: 20, right: 200, bottom: 0, left: 20},
width = 420,
height = 2050;

var start_year = 2005,
end_year = 2015;

var start_month = 1,
end_month = 12;

var c = d3.scale.category20c();

var x = d3.scale.linear()
.range([0, width]);

var xAxis = d3.svg.axis()
.scale(x)
.orient("top");

var formatYears = d3.format("0000");
xAxis.tickFormat(formatYears);

var svg = d3.select("#chart").append("svg")
			.attr("width", width + margin.left + margin.right)
			.attr("height", height + margin.top + margin.bottom)
			.style("margin-left", margin.left + "px")
			.append("g")
			.attr("transform", "translate(" + margin.left + "," + margin.top + ")");

function mouseover(p) {
	var g = d3.select(this).node().parentNode;
	d3.select(g).selectAll("circle").style("display","none");
	d3.select(g).selectAll("text.value").style("display","block");
}

function mouseout(p) {
	var g = d3.select(this).node().parentNode;
	d3.select(g).selectAll("circle").style("display","block");
	d3.select(g).selectAll("text.value").style("display","none");
}
